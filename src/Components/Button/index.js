/* eslint-disable react/button-has-type */
import React from 'react';
import PropTypes from 'prop-types';
import css from './button.module.css';

const Button = ({ text, onClick, type, width, inputType, disabled, size }) => {
  const className = `${css.button} ${size && css[`size-${size}`]} ${
    type && css[`type-${type}`]
  } ${disabled && css.disabled}`;

  return (
    <button
      style={{ width }}
      onClick={onClick}
      className={className}
      disabled={disabled}
      type={inputType}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(['primary', 'secondary']),
  width: PropTypes.string,
  inputType: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  text: '',
  onClick: () => {},
  type: 'primary',
  width: '',
  inputType: 'button',
  size: 'medium',
  disabled: false,
};

export default Button;
