import React, { useState, useEffect } from 'react';
import css from './header.module.css';
// import Text from '../Text';
import logo from 'Asset/PNG/LogoBlack.png';
import Button from 'Components/Button';
import hamburger from 'Asset/SVG/hamburger.svg';
// import Modal from './Modal';
import dynamic from 'next/dynamic';

const Modal = dynamic(() => import('./Modal'), { ssr: false });
const Text = dynamic(() => import('Components/Text'), { ssr: false });

const onClick = (element) => {
  element.scrollIntoView({ behavior: 'smooth' });
};

const Header = ({ isBreaking }) => {
  const [elements, setElements] = useState({
    whyPrimaElement: null,
    dashboardPrimaElement: null,
    servisKamiElement: null,
    contactUsElement: null,
    renderModal: false,
  });

  useEffect(() => {
    setElements((prev) => {
      return {
        ...prev,
        whyPrimaElement: document.getElementById('whyPrima'),
        dashboardPrimaElement: document.getElementById('dashboardPrima'),
        servisKamiElement: document.getElementById('servisKami'),
        contactUsElement: document.getElementById('contactUs'),
      };
    });
  }, []);
  return (
    <>
      {isBreaking && (
        <Modal
          key="modal"
          onClick={onClick}
          show={elements.renderModal}
          elements={elements}
          setElements={setElements}
        />
      )}
      <div key="header" className={css.container}>
        <div className={css.leftContainer}>
          <div className={css.logo}>
            <img src={logo} alt="Prima landing page" />
          </div>
          {isBreaking ? (
            <div></div>
          ) : (
            <>
              <div
                className={css.link}
                onClick={() => {
                  onClick(elements?.whyPrimaElement);
                }}
              >
                <Text text="Home" size="small" variant="regular" />
              </div>
              <div
                className={css.link}
                onClick={() => {
                  onClick(elements?.dashboardPrimaElement);
                }}
              >
                <Text text="About Us" size="small" variant="regular" />
              </div>
              <div
                className={css.link}
                onClick={() => {
                  onClick(elements?.servisKamiElement);
                }}
              >
                <Text text="Our Services" size="small" variant="regular" />
              </div>
            </>
          )}
        </div>
        <div className={css.rightContainer}>
          {isBreaking ? (
            <div
              role="button"
              tabIndex="0"
              onKeyDown={() => {}}
              onClick={() => {
                setElements((prev) => ({ ...prev, renderModal: true }));
              }}
              className={css.hamburgerContainer}
            >
              <img src={hamburger} alt="hamburger" />
            </div>
          ) : (
            <Button
              text="Contact Us"
              type="secondary"
              onClick={() => {
                onClick(elements?.contactUsElement);
              }}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Header;
