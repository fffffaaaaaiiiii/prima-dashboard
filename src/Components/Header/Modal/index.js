import React from 'react';
import css from './modal.module.css';
import Text from 'Components/Text';
import closeIcon from 'Asset/SVG/x-grey.svg';

const Modal = ({ show, setElements, onClick, elements }) => {
  const closeModal = () => {
    setElements((prev) => ({ ...prev, renderModal: false }));
  };
  return (
    <div
      key="modalContainer"
      style={{
        left: show ? '0%' : '100%',
      }}
      className={css.modalContainer}
      onClick={() => {
        closeModal();
      }}
      tabIndex="0"
      role="button"
      onKeyDown={() => {}}
    >
      <div className={css.modalHeader} key="modalHeader">
        <div className={css.closeButtonContainer}>
          <img src={closeIcon} alt="close" />
        </div>
      </div>
      <div key="modalContent" className={css.modalContentContainer}>
        <div
          className={css.link}
          onClick={() => {
            onClick(elements?.whyPrimaElement);
          }}
        >
          <Text text="Home" size="medium" variant="medium" />
        </div>
        <div className={css.horizontalLine}></div>
        <div
          className={css.link}
          onClick={() => {
            onClick(elements?.dashboardPrimaElement);
          }}
        >
          <Text text="About Us" size="medium" variant="medium" />
        </div>
        <div className={css.horizontalLine}></div>
        <div
          className={css.link}
          onClick={() => {
            onClick(elements?.servisKamiElement);
          }}
        >
          <Text text="Our Services" size="medium" variant="medium" />
        </div>
        <div className={css.horizontalLine}></div>
        <div
          className={css.link}
          onClick={() => {
            onClick(elements?.contactUsElement);
          }}
        >
          <Text text="Contact Us" size="medium" variant="medium" />
        </div>
      </div>
    </div>
  );
};

export default Modal;
