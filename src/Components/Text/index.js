import React from 'react';
import PropTypes from 'prop-types';
import css from './text.module.css';

const Text = ({
  text,
  size,
  block,
  color,
  variant,
  italic,
  underline,
  clickAble,
  onClick,
  marginBottom,
  textTransform,
  lineHeight,
  align,
}) => {
  const className = `${css.textClassName} ${
    textTransform && css[`textTransform-${textTransform}`]
  }
    ${size && css[`size-${size}`]} ${block && css.block} ${
    color && css[`color-${color}`]
  } ${variant && css[`variant-${variant}`]} ${italic && css.italic} ${
    underline && css.underline
  } ${clickAble && css.clickAble} ${
    marginBottom && css[`marginBottom-${marginBottom}`]
  } ${align && css[`align-${align}`]}
  `;
  return (
    <span
      style={{ lineHeight: lineHeight }}
      onClick={onClick}
      role="presentation"
      className={className}
    >
      {text}
    </span>
  );
};

Text.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  size: PropTypes.oneOf([
    'xxsmall',
    'xsmall',
    'small',
    'normal',
    'medium',
    'large',
    'xlarge',
    'xxlarge',
  ]),
  block: PropTypes.bool,
  color: PropTypes.oneOf(['primary', 'white', 'black', 'grey']),
  variant: PropTypes.oneOf([
    'thin',
    'extraLight',
    'light',
    'regular',
    'medium',
    'semiBold',
    'bold',
    'extraBold',
    'black',
  ]),
  italic: PropTypes.bool,
  underline: PropTypes.bool,
  clickAble: PropTypes.bool,
  onClick: PropTypes.func,
  marginBottom: PropTypes.oneOf([
    'none',
    'small',
    'normal',
    'medium',
    'large',
    'xlarge',
  ]),
  textTransform: PropTypes.oneOf([
    'capitalize',
    'uppercase',
    'lowercase',
    'none',
  ]),
  lineHeight: PropTypes.string,
  align: PropTypes.oneOf(['left', 'right', 'center', 'unset']),
};

Text.defaultProps = {
  text: '',
  size: 'small',
  block: false,
  color: 'black',
  variant: 'regular',
  italic: false,
  underline: false,
  clickAble: false,
  onClick: () => {},
  marginBottom: 'none',
  textTransform: 'none',
  lineHeight: 'unset',
  align: 'unset',
};

export default Text;
