/* eslint-disable no-nested-ternary */
import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import css from './input.module.css';
import Text from '../Text';

const Input = forwardRef(function Input(
  {
    name,
    id,
    type,
    placeholder,
    marginTop,
    marginBottom,
    autocomplete,
    iconLeft,
    size,
    iconRight,
    color,
    errorText,
    readOnly,
    value,
    controlled,
    onChange,
    textArea,
  },
  ref
) {
  return (
    <section
      className={`${marginBottom && css[`marginBottom-${marginBottom}`]} ${
        marginTop && css[`marginTop-${marginTop}`]
      } ${css.container} ${iconLeft && css.iconLeft} ${
        iconRight && css.iconRight
      } ${size && css[`size-${size}`]} ${color && css[`color-${color}`]} ${
        !!errorText && css.hasError
      }`}
    >
      <label htmlFor={id}>
        {iconLeft && (
          <div className={css.iconLeftContainer}>
            <img src={iconLeft} alt="iconLeft" />
          </div>
        )}
        {!textArea ? (
          <input
            className={css.input}
            onChange={controlled ? onChange : undefined}
            value={controlled ? value : undefined}
            readOnly={readOnly}
            ref={ref}
            name={name}
            autoComplete={autocomplete}
            id={id}
            type={type}
            placeholder={placeholder}
          />
        ) : (
          <textarea
            onChange={controlled ? onChange : undefined}
            value={controlled ? value : undefined}
            readOnly={readOnly}
            ref={ref}
            name={name}
            autoComplete={autocomplete}
            id={id}
            type={type}
            placeholder={placeholder}
            rows="10"
            className={`${css.textArea} ${css.input}`}
          />
        )}
        {iconRight && (
          <div className={css.iconRightContainer}>
            <img src={iconRight} alt="iconRight" />
          </div>
        )}
      </label>
      {!!errorText && (
        <div className={css.errorContainer}>
          <Text text={errorText} size="small" color="red" variant="regular" />
        </div>
      )}
    </section>
  );
});

Input.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  marginTop: PropTypes.oneOf(['none', 'small', 'medium', 'large', 'xlarge']),
  marginBottom: PropTypes.oneOf(['none', 'small', 'medium', 'large', 'xlarge']),
  autocomplete: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  readOnly: PropTypes.bool,
  value: PropTypes.string,
  onChange: PropTypes.func,
  textArea: PropTypes.bool,
};

Input.defaultProps = {
  label: '',
  name: '',
  type: 'text',
  placeholder: '',
  marginTop: 'none',
  marginBottom: 'xlarge',
  autocomplete: 'on',
  size: 'large',
  readOnly: false,
  value: '',
  onChange: () => {},
  textArea: false,
};

export default Input;
