import Head from 'next/head';
import css from '../styles/Home.module.css';
import Header from 'Components/Header/index';
import Button from 'Components/Button/index';
import truckImage from 'Asset/PNG/Image-Truck.png';
// import Text from 'Components/Text';
import dashboardImage from 'Asset/PNG/Image-Laptop.png';
import truckIcon from 'Asset/PNG/Icon-Truck.png';
import pinIcon from 'Asset/PNG/Icon-Pin.png';
import calendarIcon from 'Asset/PNG/Icon-Calendar.png';
import warehouseIcon from 'Asset/PNG/Illustration-Warehouse.png';
import truckWarehouseIcon from 'Asset/PNG/Illustration-Truck.png';
import tremIcon from 'Asset/PNG/Illustration-Trem.png';
import deliveredIcon from 'Asset/PNG/Illustration-Delivered.png';
import mapImage from 'Asset/PNG/Image-Map.png';
import Input from 'Components/Input';
import { useCustomScreenSize } from 'Function';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import statusDone from 'Asset/PNG/Status-Done.png';
import statusPassive from 'Asset/PNG/Status-Passive.png';
import dynamic from 'next/dynamic';
const Text = dynamic(() => import('Components/Text'), { ssr: false });

const sizeObject = {
  xxlarge: {
    desktop: 'xxlarge',
    mobile: 'xlarge',
  },
  xlarge: {
    desktop: 'xlarge',
    mobile: 'large',
  },
  large: {
    desktop: 'large',
    mobile: 'medium',
  },
  medium: {
    desktop: 'medium',
    mobile: 'normal',
  },
  normal: {
    desktop: 'normal',
    mobile: 'small',
  },
  small: {
    desktop: 'small',
    mobile: 'xsmall',
  },
  xsmall: {
    desktop: 'xsmall',
    mobile: 'xxsmall',
  },
  xxsmall: {
    desktop: 'xxsmall',
    mobile: 'xxsmall',
  },
};

export default function Home() {
  const breakHeader = useCustomScreenSize(1170);
  const isMobile = useCustomScreenSize(480);
  console.log(isMobile);
  const [state, setState] = useState({
    hoveredService: '',
    contactElement: null,
    awb: '',
    serverResponse: null,
    formData: {
      name: '',
      email: '',
      phone: '',
      message: '',
    },
    sendMessageStatus: '',
  });

  const handleMobileTextSize = (size) => {
    if (isMobile) {
      return sizeObject[size].mobile;
    } else {
      return sizeObject[size].desktop;
    }
  };

  const onSendMessage = () => {
    const body = {
      name: state.formData.name,
      email: state.formData.email,
      phone: state.formData.phone,
      note: state.formData.message,
    };
    axios
      .post('https://api.envio.asia/sandbox/admin/public/contact', body)
      .then((res) => {
        setState((prev) => ({ ...prev, sendMessageStatus: 'success' }));
      })
      .catch(() => {
        setState((prev) => ({
          ...prev,
          sendMessageStatus: 'Terjadi Kesalahan, Silahkan coba kembali',
        }));
      });
  };

  console.log(state.formData);

  const onLacak = () => {
    axios
      .get('https://api.envio.asia/sandbox/admin/public/tracking', {
        params: { resi_number: state.awb },
      })
      .then((res) => {
        setState((prev) => ({
          ...prev,
          serverResponse: {
            ...res.data.data,
            awb: state.awb,
          },
        }));
      })
      .catch((e) => {
        setState((prev) => ({ ...prev, serverResponse: 'error' }));
      });
  };

  const dummyTracking = {
    pickup_status: 'pending',
    sending_status: 'finished',
    delivered_status: 'pending',
  };

  useEffect(() => {
    setState((prev) => ({
      ...prev,
      contactElement: document.getElementById('contactUs'),
    }));
  }, []);

  return (
    <div className={css.highestParent}>
      <Head>
        <title>Prima</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header isBreaking={breakHeader} />
      <main>
        <div className={css.heroContainer}>
          <div className={css.textContainer}>
            <Text
              text="Partner Logistik Anda"
              variant="black"
              size={handleMobileTextSize('xxlarge')}
              color="white"
            />
            <div className={css.pill} />
            <Text
              lineHeight="3rem"
              variant="light"
              size="medium"
              color="white"
              text="Prima logistics adalah perusahaan logistik berbasis teknologi yang menghubungkan antar bisnis maupun bisnis dengan pelanggan. Kami mengkombinasikan teknologi dengan pelayanan prima."
            />
            <div className={css.buttonContainer}>
              <Button
                text="Connect with us"
                size="large"
                onClick={() => {
                  state.contactElement.scrollIntoView({ behavior: 'smooth' });
                }}
              />
            </div>
          </div>
          <div className={css.background} />
        </div>
        <section className={css.lacakContainer}>
          <div className={css.lacakInputContainer}>
            <Text
              text="Lacak Pesanan Anda"
              size={'xlarge'}
              variant="black"
              align="center"
            />
            <div className={css.pill} />
            <Text
              block
              marginBottom="large"
              text="Masukkan nomor resi di kolom di bawah ini untuk melakukan pelacakan."
              size={handleMobileTextSize('medium')}
              variant="regular"
            />
            <Input
              size="large"
              controlled
              placeholder="Nomor Resi"
              value={state.awb}
              onChange={(e) => {
                e.persist();
                setState((prev) => {
                  return {
                    ...prev,
                    awb: e.nativeEvent.target.value,
                  };
                });
              }}
            />
            <div className={css.lacakButtonContainer}>
              <Button
                text="Lacak Sekarang"
                disabled={!state.awb}
                onClick={() => {
                  onLacak(state.awb);
                }}
                size="large"
                width="100%"
              />
            </div>
          </div>
          <div className={css.horizontalLine}></div>
          <div
            className={
              state.serverResponse
                ? state.serverResponse === 'error'
                  ? css.showLacakResponseError
                  : css.showLacakResponse
                : css.hideLacakResponse
            }
          >
            <div className={css.lacakResultContainer}>
              <Text
                text="Hasil Lacak Pesanan"
                align="center"
                size={'xlarge'}
                variant="black"
              />
              <div className={css.pill} />
              <div className={css.lacakResultCard}>
                {state.serverResponse && state.serverResponse !== 'error' ? (
                  <>
                    <div className={css.lacakResultCardHeader}>
                      <Text
                        size="normal"
                        variant="black"
                        text="Nomor Resi&nbsp;&nbsp;&nbsp;&nbsp;"
                      />
                      <Text
                        size="normal"
                        variant="semiBold"
                        text={state.serverResponse.awb}
                      />
                    </div>
                    <div className={css.lacakResultCardBody}>
                      <div>
                        <Text
                          block
                          text="Nama Penerima"
                          color="primary"
                          size="small"
                          variant="black"
                          marginBottom="small"
                        />
                        <Text
                          block
                          text={state.serverResponse.receipt_name}
                          size="normal"
                          variant="medium"
                          marginBottom="large"
                          color="grey"
                        />
                        <Text
                          block
                          text="Alamat Penerima"
                          color="primary"
                          size="small"
                          variant="black"
                          marginBottom="small"
                        />
                        <Text
                          block
                          text={state.serverResponse.receipt_address}
                          size="normal"
                          variant="medium"
                          color="grey"
                        />
                      </div>
                      <div className={css.lacakStatusContainer}>
                        <div className={css.lacakPickupContainer}>
                          <div
                            className={
                              state.serverResponse.pickup_status === 'finished'
                                ? css.doneIconContainer
                                : css.pendingIconContainer
                            }
                          >
                            <img
                              src={
                                state.serverResponse.pickup_status ===
                                'finished'
                                  ? statusDone
                                  : statusPassive
                              }
                              alt="done"
                            />
                          </div>
                          <Text block text="Pickup" size="small" color="grey" />
                        </div>
                        <div className={css.lacakVerticalLine}></div>
                        <div className={css.lacakPickupContainer}>
                          <div
                            className={
                              state.serverResponse.sending_status === 'finished'
                                ? css.doneIconContainer
                                : css.pendingIconContainer
                            }
                          >
                            <img
                              src={
                                state.serverResponse.sending_status ===
                                'finished'
                                  ? statusDone
                                  : statusPassive
                              }
                              alt="done"
                            />
                          </div>
                          <Text
                            block
                            text="On going"
                            size="small"
                            color="grey"
                          />
                        </div>
                        <div className={css.lacakVerticalLine}></div>
                        <div className={css.lacakPickupContainer}>
                          <div
                            className={
                              state.serverResponse.delivered_status ===
                              'finished'
                                ? css.doneIconContainer
                                : css.pendingIconContainer
                            }
                          >
                            <img
                              src={
                                state.serverResponse.delivered_status ===
                                'finished'
                                  ? statusDone
                                  : statusPassive
                              }
                              alt="done"
                            />
                          </div>
                          <Text
                            block
                            text="Delivered"
                            size="small"
                            color="grey"
                          />
                        </div>
                      </div>
                    </div>
                  </>
                ) : (
                  <div className={css.lacakResultErrorContainer}>
                    <Text
                      block
                      text="Nomor resi yang Anda masukkan tidak ditemukan"
                      size="medium"
                      variant="regular"
                      color="grey"
                    />
                  </div>
                )}
              </div>
            </div>
            <div className={css.horizontalLine}></div>
          </div>
        </section>
        <section className={css.whyPrimaContainer} id="whyPrima">
          {isMobile ? (
            <>
              <div className={css.whyPrimaImage}>
                <img
                  className={css.truckImage}
                  src={truckImage}
                  alt="Why Prima ?"
                />
              </div>
              <div className={css.whyPrimaText}>
                <Text text="Why Prima" size="xlarge" variant="black" />
                <div className={css.pill} />
                <Text
                  block
                  marginBottom="large"
                  lineHeight="3.5rem"
                  size="small"
                  text="Layanan kami menjangkau untuk pengiriman antar kota dan seluruh wilayah Indonesia yang cepat, handal, dan dapat diukur untuk bisnis dari semua ukuran di Wilayah Jabodetabek."
                />
                <Text
                  block
                  lineHeight="3.5rem"
                  size="small"
                  text="Sejak 2015, Prima telah dipercaya oleh ribuan perusahaan, UKM hingga korporasi dalam memberikan produk mereka kepada pelanggan di seluruh Indonesia."
                />
              </div>
            </>
          ) : (
            <>
              <div className={css.whyPrimaText}>
                <Text text="Why Prima" size="xlarge" variant="black" />
                <div className={css.pill} />
                <Text
                  block
                  marginBottom="large"
                  lineHeight="3.5rem"
                  size="medium"
                  text="Layanan kami menjangkau untuk pengiriman antar kota dan seluruh wilayah Indonesia yang cepat, handal, dan dapat diukur untuk bisnis dari semua ukuran di Wilayah Jabodetabek."
                />
                <Text
                  block
                  lineHeight="3.5rem"
                  size="medium"
                  text="Sejak 2015, Prima telah dipercaya oleh ribuan perusahaan, UKM hingga korporasi dalam memberikan produk mereka kepada pelanggan di seluruh Indonesia."
                />
              </div>
              <div className={css.whyPrimaImage}>
                <img
                  className={css.truckImage}
                  src={truckImage}
                  alt="Why Prima ?"
                />
              </div>
            </>
          )}
        </section>
        <section className={css.dashboardPrimaContainer} id="dashboardPrima">
          <div className={css.backgroundGrey} />
          <div className={css.dashboardImageContainer}>
            <img src={dashboardImage} alt="Prima Dashboard" />
          </div>
          <div className={css.dashboardTextContainer}>
            <Text
              lineHeight="4rem"
              text="Dashboard Prima"
              color="primary"
              variant="black"
              size={handleMobileTextSize('medium')}
            />
            <Text
              color="grey"
              lineHeight="4rem"
              size={handleMobileTextSize('medium')}
              variant="medium"
              text=" adalah sistem manajerial paket yang memudahkan Anda untuk mengirim, melacak dan mengelola data pengiriman paket Anda. Tidak lagi perlu menghabiskan waktu untuk mencatat secara menual untuk pelacakan ataupun mengelola pengiriman, semua dilakukan secara otomatis melalui sistem."
            />
          </div>
        </section>
        <section className={css.fiturDashboardPrimaContainer}>
          <Text
            text="Fitur Dashboard Prima"
            align="center"
            variant="black"
            size="xlarge"
          />
          <div className={css.pill} />
          <div className={css.fiturFiturDashboard}>
            <div className={css.singleFitur}>
              <img src={truckIcon} alt="Pengiriman" />
              <Text
                lineHeight="3rem"
                text="Pengiriman"
                variant="black"
                size="medium"
                block
              />
              <Text
                lineHeight="3rem"
                size="medium"
                text="Membuat order baru jadi lebih mudah, cukup input ke dalam sistem yang tersedia."
              />
            </div>
            <div className={css.singleFitur}>
              <img src={pinIcon} alt="Pengiriman" />
              <Text
                lineHeight="3rem"
                text="Pelacakan"
                variant="black"
                size="medium"
                block
              />
              <Text
                lineHeight="3rem"
                size="medium"
                text="Ketahui, lacak, dan pantu paketmu secara real time. Agar Anda tahu tepat posisi paket berada."
              />
            </div>
            <div className={css.singleFitur}>
              <img src={calendarIcon} alt="Pengiriman" />
              <Text
                lineHeight="3rem"
                text="Pengelolaan"
                variant="black"
                size="medium"
                block
              />
              <Text
                lineHeight="3rem"
                size="medium"
                text="Kelola pesanan paket Anda di waktu yang sama jadi lebih mudah dengan sistem kami."
              />
            </div>
          </div>
        </section>
        <section className={css.servisKamiContainer} id="servisKami">
          <Text
            text="Servis Kami"
            variant="black"
            align="center"
            size="xlarge"
          />
          <div className={css.pill} />
          <div className={css.servisServisContainer}>
            <div
              className={`${css.singleServis} ${
                state.hoveredService === 'warehouse'
                  ? css.hoveredServis
                  : css.normalServis
              }`}
              onMouseEnter={() => {
                setState((prev) => ({ ...prev, hoveredService: 'warehouse' }));
              }}
              onMouseLeave={() => {
                setState((prev) => ({ ...prev, hoveredService: '' }));
              }}
            >
              {state.hoveredService === 'warehouse' ? (
                <>
                  <Text
                    text="Warehouse & Fulfillment"
                    size="large"
                    variant="black"
                    color="white"
                    block
                    marginBottom="large"
                  />
                  <Text
                    size="normal"
                    variant="regular"
                    color="white"
                    text="Penyimpanan stock dan barang yang dilengkapi pengemasan, packing dan pengiriman automatis untuk setiap pemesanan. "
                    block
                  />
                </>
              ) : (
                <>
                  <img src={warehouseIcon} alt="Warehouse and Fulfillment" />
                  <Text
                    text="Warehouse & Fulfillment"
                    size="medium"
                    variant="black"
                  />
                </>
              )}
            </div>
            <div
              className={`${css.singleServis} ${
                state.hoveredService === 'sorting'
                  ? css.hoveredServis
                  : css.normalServis
              }`}
              onMouseEnter={() => {
                setState((prev) => ({ ...prev, hoveredService: 'sorting' }));
              }}
              onMouseLeave={() => {
                setState((prev) => ({ ...prev, hoveredService: '' }));
              }}
            >
              {state.hoveredService === 'sorting' ? (
                <>
                  <Text
                    text="Sorting and Consolidation"
                    size="large"
                    variant="black"
                    color="white"
                    marginBottom="large"
                  />
                  <Text
                    size="normal"
                    variant="regular"
                    color="white"
                    text="Solusi pengiriman hemat untuk barang bermuatan berat se-Nusantara"
                  />
                </>
              ) : (
                <>
                  <img
                    src={truckWarehouseIcon}
                    alt="Sorting and Consolidation"
                  />
                  <Text
                    text="Sorting & Consolidation"
                    size="medium"
                    variant="black"
                  />
                </>
              )}
            </div>
            <div
              className={`${css.singleServis} ${
                state.hoveredService === 'supply'
                  ? css.hoveredServis
                  : css.normalServis
              }`}
              onMouseEnter={() => {
                setState((prev) => ({ ...prev, hoveredService: 'supply' }));
              }}
              onMouseLeave={() => {
                setState((prev) => ({ ...prev, hoveredService: '' }));
              }}
            >
              {state.hoveredService === 'supply' ? (
                <>
                  <Text
                    text="Supply Chain & Distribution"
                    size="large"
                    variant="black"
                    color="white"
                    marginBottom="large"
                  />
                  <Text
                    size="normal"
                    variant="regular"
                    color="white"
                    text="Solusi distribusi untuk usaha anda mulai dari UMKM hingga Industri Spesifik termasuk General Cargo dan Cold Chain."
                  />
                </>
              ) : (
                <>
                  <img src={tremIcon} alt="Supply Chain & Distribution" />
                  <Text
                    text="Supply Chain & Distribution"
                    size="medium"
                    variant="black"
                  />
                </>
              )}
            </div>
            <div
              className={`${css.singleServis} ${
                state.hoveredService === 'lastMile'
                  ? css.hoveredServis
                  : css.normalServis
              }`}
              onMouseEnter={() => {
                setState((prev) => ({ ...prev, hoveredService: 'lastMile' }));
              }}
              onMouseLeave={() => {
                setState((prev) => ({ ...prev, hoveredService: '' }));
              }}
            >
              {state.hoveredService === 'lastMile' ? (
                <>
                  <Text
                    text="Last Mile Delivery"
                    size="large"
                    variant="black"
                    color="white"
                    marginBottom="large"
                  />
                  <Text
                    size="normal"
                    variant="regular"
                    color="white"
                    text="Pengiriman Cepat se-Nusantara  melalui kurir dan partner logistik terkemuka."
                  />
                </>
              ) : (
                <>
                  <img src={deliveredIcon} alt="Last Mile Delivery" />
                  <Text
                    text="Last Mile Delivery"
                    size="medium"
                    variant="black"
                  />
                </>
              )}
            </div>
          </div>
        </section>
        <section className={css.jaringanContainer}>
          <Text
            text="Jaringan yang terpadu dengan teknologi"
            variant="black"
            align="center"
            size="xlarge"
          />
          <div className={css.pill} />
          <div className={css.jaringanTextContainer}>
            <Text
              lineHeight="3rem"
              size={handleMobileTextSize('medium')}
              text="Didukung lebih dari 7000 Agen Prima dan mitra logistik ternama, Prima Logistics mencakup pengiriman berbagai macam barang hingga ke seluruh wilayah Indonesia."
            />
          </div>
          <div className={css.mapContainer}>
            <img src={mapImage} alt="Jaringan yang terpadu dengan teknologi" />
          </div>
        </section>
        <section className={css.contactUsContainer} id="contactUs">
          <Text
            text="Contact Us"
            variant="black"
            align="center"
            size="xlarge"
          />
          <div className={css.pill} />
          <div className={css.inputsContainer}>
            <Input
              onChange={(e) => {
                e.persist();
                setState((prev) => ({
                  ...prev,
                  sendMessageStatus: '',
                  formData: {
                    ...prev.formData,
                    name: e.target.value,
                  },
                }));
              }}
              controlled
              value={state.formData.name}
              size="large"
              placeholder="Nama"
            />
            <Input
              onChange={(e) => {
                e.persist();
                setState((prev) => ({
                  ...prev,
                  sendMessageStatus: '',
                  formData: {
                    ...prev.formData,
                    email: e.target.value,
                  },
                }));
              }}
              controlled
              value={state.formData.email}
              name="email"
              size="large"
              placeholder="Email"
              type="email"
            />
            <Input
              onChange={(e) => {
                e.persist();
                setState((prev) => ({
                  ...prev,
                  sendMessageStatus: '',
                  formData: {
                    ...prev.formData,
                    phone: e.target.value,
                  },
                }));
              }}
              controlled
              value={state.formData.phone}
              size="large"
              placeholder="Nomor Telepon"
              type="tel"
            />
          </div>
          <Input
            onChange={(e) => {
              e.persist();
              setState((prev) => ({
                ...prev,
                sendMessageStatus: '',
                formData: {
                  ...prev.formData,
                  message: e.target.value,
                },
              }));
            }}
            controlled
            value={state.formData.message}
            size="large"
            placeholder="Isi Pesan"
            textArea
          />
          <div className={css.contactButtonContainer}>
            <Button
              onClick={() => {
                onSendMessage();
              }}
              width="100%"
              size="large"
              text="Kirim Pesan Sekarang"
            />
          </div>
          {state.sendMessageStatus && (
            <div
              className={`${css.notification} ${
                state.sendMessageStatus === 'success' ? css.success : css.error
              }`}
            >
              <Text
                text={
                  state.sendMessageStatus === 'success'
                    ? 'Pesan Berhasil Dikirim'
                    : state.sendMessageStatus
                }
                color="white"
                size="small"
                variant="medium"
              />
            </div>
          )}
        </section>
      </main>
      <footer className={css.footerContainer}>
        <Text
          text="Copyright © 2020 Prima Logistik"
          color="white"
          size="small"
        />
      </footer>
    </div>
  );
}
