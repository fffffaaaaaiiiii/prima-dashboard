import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <Component {...pageProps} />
      <style jsx global>{`
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 100;
          font-display: swap;
          src: url('/fonts/Inter/Inter-Thin.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 100;
          font-display: swap;
          src: url('/fonts/Inter/Inter-ThinItalic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 200;
          font-display: swap;
          src: url('/fonts/Inter/Inter-ExtraLight.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 200;
          font-display: swap;
          src: url('/fonts/Inter/Inter-ExtraLightItalic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 300;
          font-display: swap;
          src: url('/fonts/Inter/Inter-Light.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 300;
          font-display: swap;
          src: url('/fonts/Inter/Inter-LightItalic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 400;
          font-display: swap;
          src: url('/fonts/Inter/Inter-Regular.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 400;
          font-display: swap;
          src: url('/fonts/Inter/Inter-Italic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 500;
          font-display: swap;
          src: url('/fonts/Inter/Inter-Medium.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 500;
          font-display: swap;
          src: url('/fonts/Inter/Inter-MediumItalic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 600;
          font-display: swap;
          src: url('/fonts/Inter/Inter-SemiBold.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 600;
          font-display: swap;
          src: url('/fonts/Inter/Inter-SemiBoldItalic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 700;
          font-display: swap;
          src: url('/fonts/Inter/Inter-Bold.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 700;
          font-display: swap;
          src: url('/fonts/Inter/Inter-BoldItalic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 800;
          font-display: swap;
          src: url('/fonts/Inter/Inter-ExtraBold.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 800;
          font-display: swap;
          src: url('/fonts/Inter/Inter-ExtraBoldItalic.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: normal;
          font-weight: 900;
          font-display: swap;
          src: url('/fonts/Inter/Inter-Black.woff2');
        }
        @font-face {
          font-family: 'Inter';
          font-style: italic;
          font-weight: 900;
          font-display: swap;
          src: url('/fonts/Inter/Inter-BlackItalic.woff2');
        }
      `}</style>
    </div>
  );
}

export default MyApp;
